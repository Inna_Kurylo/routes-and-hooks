import { useState, useEffect } from 'react';
import { Route, Routes } from "react-router-dom";

import Header from './components/Header';
import { Modal } from './components/Modal';
import Footer from './components/Footer/Footer';
import Home from './pages/Home';
import Cart from './pages/Cart';
import Favorite from './pages/Favorite';

function App() {
  const [isModal, setIsModal] = useState(false);
  const [cards, setCards] = useState([]);
  const [cart, setCart] = useState([])
  const [currentProd, setCurrentProd] = useState({});
  const [favorite, setFavorite] = useState([]);

  useEffect(() => {
    fetch("phones.json")
      .then((response) => response.json())
      .then((data) => {
        setCards(data);
        setCart(JSON.parse(localStorage.getItem('cart') || '[]'));
        setFavorite(JSON.parse(localStorage.getItem('favorite') || '[]'))
        console.log('state');
      })
  }, []);

  const handlerModal = () => {
    setIsModal(!isModal)
  };

  const handlerCurrentProd = (currentProd) => {
    setCurrentProd({ ...currentProd })
  }

  const handlerFavorite = (heart) => {
    const isAdd = favorite.some(row => row.articul === heart.articul);
    let favoriteToggle;
    if (isAdd) {
      favoriteToggle = favorite.filter(row => row.articul !== heart.articul);
    } else {
      favoriteToggle = [...favorite, heart];
    }
    localStorage.setItem('favorite', JSON.stringify(favoriteToggle))
    setFavorite(favoriteToggle)
  }
  const handlerDeleteCart = (del) => {

    const isDel = cart.filter(row => row.articul !== del.articul)
    setCart(isDel);
    localStorage.setItem('cart', JSON.stringify(isDel));

  }
  const handlerCart = (currentProd) => {
    const isAddCart = cart.some(row => row.articul === currentProd.articul);
    if (!isAddCart) {
      const cartStorage = [...cart, currentProd];
      localStorage.setItem('cart', JSON.stringify(cartStorage))
      setIsModal(!isModal)
      setCart(cartStorage)
    } else handlerModal();
  }
  const action = (
    <div className="button-wrapper">
      <button className="btn" type="button" onClick={
        !currentProd.inCart ? () => handlerCart(currentProd) : () => {
          handlerModal()
          handlerDeleteCart(currentProd)
        }
      } >OK</button>
      <button className="btn" type="button" onClick={() => handlerModal()}>Cancel</button>
    </div>)

  const cardUpDate = cards.map(row => {
    return {
      ...row,
      inFav: favorite.some(dataCard => dataCard.articul === row.articul),
    };
  });
  const textBuy = 'Buy';
  const textToCart = 'To cart';

  const cartUpDate = cart.map(row => {
    return {
      ...row,
      inCart: cart.some(dataCart => dataCart.articul === row.articul),
    };
  });
  let textModal = isModal && currentProd.inCart ? "Do you want delete this product from the cart?" : "Do you want to add a product to your cart?"
  return (
    <div className="page__wrapper">
      <Header count={cart.length} countFav={favorite.length} />
      <main className="main">
        <Routes>
          <Route path="/" element={<Home text={textToCart} cards={cardUpDate} handlerCurrentProd={handlerCurrentProd} handlerModal={handlerModal} handlerFavorite={handlerFavorite} />} />
          <Route path="/favorites" element={<Favorite text={textToCart} favorite={favorite} handlerFavorite={handlerFavorite} handlerCurrentProd={handlerCurrentProd} handlerModal={handlerModal}/>} />
          <Route path="/cart" element={<Cart cart={cartUpDate} text={textBuy} handlerModal={handlerModal} handlerCurrentProd={handlerCurrentProd} />} />
        </Routes>
      </main>
      <Footer />
      {isModal && <Modal textModal={textModal} action={action} closeModal={handlerModal} title={currentProd.name} />}

    </div>
  )
}
export default App;