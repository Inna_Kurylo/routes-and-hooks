import PropTypes from 'prop-types';
import Card from "../../components/AllCards/components/Card";
import Button from "../../components/Button"
function Cart({handlerCurrentProd, handlerModal, cart, text}){
    const cartPages = cart.map((item, index) => <Card 
                                                handlerCurrentProd={handlerCurrentProd} 
                                                handlerModal={handlerModal}   
                                                key={index} data={item} />)
    return(
        <div className="wrap">
                <div className="cards" >
                    {cartPages}
                    <Button text = {text}/>
                </div>
                
        </div>
    )
}
Cart.propTypes = {
	handlerCurrentProd: PropTypes.func,
    handlerModal: PropTypes.func,
    text: PropTypes.string,
    cart: PropTypes.array
};
export default Cart;