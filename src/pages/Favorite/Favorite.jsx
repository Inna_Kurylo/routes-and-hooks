import PropTypes from 'prop-types';
import Card from "../../components/AllCards/components/Card";

function Favorite({handlerFavorite, favorite, handlerModal, handlerCurrentProd, text}){
    const favoritePages = favorite.map((item, index) => <Card 
                                                handlerFavorite={handlerFavorite}
                                                handlerCurrentProd={handlerCurrentProd}
                                                handlerModal={handlerModal}  
                                                text={text}
                                                key={index} data={{...item, inFav:true}} />)
    return(
        <div className="wrap">
                <div className="cards" >
                    {favoritePages}
                </div>
                
        </div>
    )
} 
Favorite.propTypes = {
	handlerCurrentProd: PropTypes.func,
    handlerModal: PropTypes.func,
    handlerFavorite: PropTypes.func,
    text: PropTypes.string,
    favorite: PropTypes.array
};
export default Favorite;